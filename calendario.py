#!/usr/bin/python3

calendar = {}

def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

def get_time(atime):
    """Devuelve todas las actividades para la hora atime como una lista de tuplas"""
    activities: list = {}
    for clave in calendar:
        for valor in calendar[clave]:
            if valor == atime:
                actividad = (clave, valor, calendar[clave][valor])
                activities.append(actividad)
    return activities

def get_all():
    """Devuelve todas las actividade sen el calendario como una lista de tuplas"""
    all: list = {}
    for clave in calendar:
        for valor in calendar[clave]:
            actividad = clave, valor, calendar[clave][valor]
            all.append(actividad)
    return all

def get_busiest():
    """Devuelve la fecha con más actividades, y su número de actividades"""
    busiest = None
    busiest_no = 0
    for date, times in calendar.items():
        activities_count = len(times)
        if activities_count > busiest_no:
            busiest = date
            busiest_no = activities_count
    return (busiest, busiest_no)

def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""
    fecha: list = date.split("-")
    if len(fecha) == 3 and len(fecha[0]) == 4 and len(fecha[1]) == 2 and len(fecha[2]) == 2 and 0000 <= int(fecha[0]) <= 2100 and 0 < int(fecha[1]) <= 12 and 0 < int(fecha[2]) <= 31:
        return True
    else:
        return False

def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    hora = time.split(":")
    if len(hora) == 2 and len(hora[0]) == 2 and len(hora[1]) == 2 and 00 <= int(hora[0]) <= 23 and 00 <= int(hora[1]) <= 59:
        return True
    else:
        return False

def get_activity():
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    seguir = True
    while seguir:
        print("Fecha: ", end="")
        fecha = str(input())
        if check_date(fecha):
            print("Hora: ", end="")
            hora = str(input())
            if check_time(hora):
                print("Actividad: ", end="")
                actividad = str(input())
                seguir = False
    return (fecha, hora, actividad)

def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option

def run_option(option):
    """Ejecuta el código necesario para la opción dada"""
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
    elif option == 'B':
        activities = get_all()
        show(activities)
    elif option == 'C':
        busiest, busiest_no = get_busiest()
        print(f"El día más ocupado es {busiest} con {busiest_no} actividades.")
    elif option == 'D':
        atime = input("Introduce la hora (hh:mm): ")
        activities = get_time(atime)
        show(activities)

def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()
